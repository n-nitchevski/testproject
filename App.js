/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  SafeAreaView,
  TouchableOpacity
} from 'react-native';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      basePlayerList: [
        {
          name: 'Rafael Nadal',
          photo: require('./assets/player1.jpg'),
          gender: 'Male',
          speciality: 'Super star'
        },
        {
          name: 'Novak Djokovic',
          photo: require('./assets/player2.jpg'),
          gender: 'Male',
          speciality: 'Super star'
        },
        {
          name: 'Roger Federer',
          photo: require('./assets/player5.png'),
          gender: 'Male',
          speciality: 'Super star'
        },
        {
          name: 'Andre Agassi',
          photo: require('./assets/player6.jpeg'),
          gender: 'Male',
          speciality: 'Professional'
        },
        {
          name: 'Andy Murray',
          photo: require('./assets/player7.jpeg'),
          gender: 'Male',
          speciality: 'Professional'
        },
        {
          name: 'Ivan Lendl',
          photo: require('./assets/player8.jpeg'),
          gender: 'Male',
          speciality: 'Professional'
        },
        {
          name: 'Lleyton Hewitt',
          photo: require('./assets/player9.jpeg'),
          gender: 'Male',
          speciality: 'Regular'
        },
        {
          name: 'Andy Roddick',
          photo: require('./assets/player10.jpg'),
          gender: 'Male',
          speciality: 'Regular'
        },
        {
          name: 'Rod Laver',
          photo: require('./assets/player11.jpeg'),
          gender: 'Male',
          speciality: 'Regular'
        },
        {
          name: 'Serena Williams',
          photo: require('./assets/player3.jpeg'),
          gender: 'Female',
          speciality: 'Super star'
        },
        {
          name: 'Maria Sharapova',
          photo: require('./assets/player4.jpeg'),
          gender: 'Female',
          speciality: 'Super star'
        },
        {
          name: 'Venus Williams',
          photo: require('./assets/player12.jpeg'),
          gender: 'Female',
          speciality: 'Professional'
        },
        {
          name: 'Martina Navratilova',
          photo: require('./assets/player13.jpg'),
          gender: 'Female',
          speciality: 'Professional'
        },
        {
          name: 'Steffi Graf',
          photo: require('./assets/player14.jpeg'),
          gender: 'Female',
          speciality: 'Regular'
        },
        {
          name: 'Caroline Wozniacki',
          photo: require('./assets/player15.jpeg'),
          gender: 'Female',
          speciality: 'Regular'
        }
      ],
      specialityLevels: ['Any', 'Regular', 'Professional', 'Super star'],
      genders: ['Any', 'Male', 'Female'],
      keyWord: 0,
      genderKeyword: 0,
      filters: ['Gender', 'Speciality'],
      currentFilter: 0,
      dropDownVisible: false,
      filterDropDownVisible: false
    }
  }

  render() {
    const {
      keyWord,
      genderKeyword,
      filters,
      currentFilter,
      dropDownVisible,
      filterDropDownVisible,
      basePlayerList,
      genders,
      specialityLevels
    } = this.state;
    let filteredPlayerList = [];
    if (genderKeyword !== 0) {
      filteredPlayerList = [...basePlayerList].filter(item => item.gender.indexOf(genders[genderKeyword]) >= 0);
    } else {
      filteredPlayerList = [...basePlayerList];
    }
    if (keyWord !== 0) {
      filteredPlayerList = [...filteredPlayerList].filter(item => item.speciality.indexOf(specialityLevels[keyWord]) >= 0);
    }
    let tempGenders = [...genders];
    tempGenders.splice(genderKeyword, 1);
    let tempSpecialities = [...specialityLevels];
    tempSpecialities.splice(keyWord, 1);
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.textFieldContainer}>
          <View>
            <TouchableOpacity
              onPress={() => this.setState({ dropDownVisible: !dropDownVisible })}
              style={[styles.dropDownButtonStyle, dropDownVisible && { borderBottomLeftRadius: 0, borderBottomRightRadius: 0 }]}>
              <Text style={styles.textStyle}>{filters[currentFilter]}</Text>
            </TouchableOpacity>
            {dropDownVisible && <TouchableOpacity
              onPress={() => this.setState({ currentFilter: 1 - currentFilter, dropDownVisible: false })}
              style={[styles.dropDownButtonStyle, styles.availableOptionsStyle]}>
              <Text style={styles.textStyle}>{filters[1 - currentFilter]}</Text>
            </TouchableOpacity>}
          </View>
          <View style={[styles.flexStyle, { marginLeft: 20 }]}>
            {currentFilter === 0 ?
              <TouchableOpacity
                onPress={() => this.setState({ filterDropDownVisible: !filterDropDownVisible })}
                style={[styles.dropDownButtonStyle, filterDropDownVisible && { borderBottomLeftRadius: 0, borderBottomRightRadius: 0 }]}>
                <Text style={styles.textStyle}>{genders[genderKeyword]}</Text>
              </TouchableOpacity>
            :
              <TouchableOpacity
                onPress={() => this.setState({ filterDropDownVisible: !filterDropDownVisible })}
                style={[styles.dropDownButtonStyle, filterDropDownVisible && { borderBottomLeftRadius: 0, borderBottomRightRadius: 0 }]}>
                <Text style={styles.textStyle}>{specialityLevels[keyWord]}</Text>
              </TouchableOpacity>}
            
            {filterDropDownVisible && currentFilter === 0 && tempGenders.map((item, index) => {
              return (
                <TouchableOpacity
                  key={index + 100}
                  onPress={() => this.setState({ genderKeyword: genders.indexOf(item), filterDropDownVisible: false })}
                  style={[styles.dropDownButtonStyle, styles.availableOptionsStyle, index !== (tempGenders.length - 1) && styles.availableOptionsTopStyle]}>
                  <Text style={styles.textStyle}>{item}</Text>
                </TouchableOpacity>
              );
            })}

            {filterDropDownVisible && currentFilter === 1 && tempSpecialities.map((item, index) => {
              return (
                <TouchableOpacity
                  key={index + 100}
                  onPress={() => this.setState({ keyWord: specialityLevels.indexOf(item), filterDropDownVisible: false })}
                  style={[styles.dropDownButtonStyle, styles.availableOptionsStyle, index !== (tempSpecialities.length - 1) && styles.availableOptionsTopStyle]}>
                  <Text style={styles.textStyle}>{item}</Text>
                </TouchableOpacity>
              );
            })}
          </View>
        </View>
        <ScrollView contentContainerStyle={styles.padding20}>
          {filteredPlayerList.map((item, index) => {
            return (
              <View key={index} style={styles.playerContainer}>
                <Image
                  style={styles.avatarImageStyle}
                  resizeMode={'cover'}
                  source={item.photo}/>
                <View style={[styles.flexStyle, styles.padding20]}>
                  <Text style={styles.nameStyle}>{item.name}</Text>
                  <Text>{item.speciality}</Text>
                </View>
              </View>
            );
          })}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  },
  textFieldContainer: {
    flexDirection: 'row',
    padding: 20,
  },
  padding20: {
    padding: 20
  },
  flexStyle: {
    flex: 1
  },
  textFieldStyle: {
    height: 40,
    marginRight: 20,
    backgroundColor: '#FFF',
    borderRadius: 20,
    borderColor: '#AAA',
    borderWidth: 1,
    paddingHorizontal: 20
  },
  dropDownButtonStyle: {
    height: 40,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: '#AAA',
    backgroundColor: '#DDF',
    paddingHorizontal: 15,
    justifyContent: 'center'
  },
  availableOptionsStyle: {
    borderTopWidth: 0,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    backgroundColor: '#FFF'
  },
  availableOptionsTopStyle: {
    borderBottomWidth: 1,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0
  },
  textStyle: {
    textAlign: 'center'
  },
  avatarImageStyle: {
    width: 60,
    height: 60,
    borderRadius: 30
  },
  playerContainer: {
    height: 80,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#AAA',
    borderBottomWidth: 1
  },
  nameStyle: {
    fontSize: 16,
    fontWeight: '500',
    color: '#044',
    marginBottom: 5
  }
});
